package com.akademiakodu.todolist;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodoItemAdapter extends RecyclerView.Adapter<TodoItemAdapter.TodoViewHolder> {
    private List<TodoTask> mData = Collections.emptyList();
    private OnTaskClickListener mListener;

    public TodoItemAdapter(OnTaskClickListener listener) {
        mListener = listener;
    }

    public void setData(List<TodoTask> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public TodoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rowView = inflater.inflate(R.layout.row_todoitem, parent, false);
        return new TodoViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(TodoViewHolder holder, int position) {
        TodoTask task = mData.get(position);

        // Ustawienie paska priorytetu
        if (task.isPriority()) {
            holder.itemView.setBackgroundColor(0x20FF0000);
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
        // LUB za pomocą operatora warunkowego :
//        holder.mStripe.setBackgroundColor(task.isPriority() ? Color.RED : Color.TRANSPARENT);

        // Ustawienie checkboxa DONE
        holder.mDone.setOnCheckedChangeListener(null);
        holder.mDone.setChecked(task.isDone());
        holder.mDone.setOnCheckedChangeListener(holder);

        // Ustawienie tytulu zadania
        holder.mTitle.setText(task.getTitle());

        holder.mCurrentItem = task;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class TodoViewHolder extends RecyclerView.ViewHolder implements
            CompoundButton.OnCheckedChangeListener {
        @BindView(R.id.stripe)
        View mStripe;
        @BindView(R.id.done)
        CheckBox mDone;
        @BindView(R.id.task_title)
        TextView mTitle;

        TodoTask mCurrentItem;

        public TodoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        void onClick() {
            mListener.onTaskClick(mCurrentItem);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mListener.onTaskDoneChanged(mCurrentItem, isChecked);
        }
    }

    public interface OnTaskClickListener {
        void onTaskClick(TodoTask task);

        void onTaskDoneChanged(TodoTask task, boolean done);
    }
}







