package com.akademiakodu.todolist;

import java.util.List;

public interface TodoItemsRepository {
    List<TodoTask> getTodoList();

    void save(TodoTask task);

    TodoTask findById(int objId);
}
